package hellopackage;//must be first line, even before imports
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
//import java.util.*;

public class Greeter{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Enter an integer value");
         int userValue = sc.nextInt();
        Utilities util = new Utilities();
        int multiplied = util.doubleMe(userValue);
        System.out.println("Your value multiplied:" + multiplied);

    }
    
}